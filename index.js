const express = require('express');
const Razorpay = require('razorpay');
const bodyParser = require('body-parser');
const {LocalStorage} = require("node-localstorage");
const fs = require('fs');

let app = express();
var localStorage = new LocalStorage('./scratch');

var jsonParser = bodyParser.json();

const razorpay = new Razorpay({
  key_id: 'rzp_test_yBcfP17iuVrwSY',
  key_secret: 'LEhGNx5ingxqSLgNd0wK51Nu',

})
app.set('views', 'views')
app.set('view engine', 'ejs')
app.use(express.urlencoded(
  {
    extended: true
  })
)
// app.use(bodyParser.json())

app.get('/',(req, res) =>{
  res.render('order.ejs')
})
app.get('/thank-you',(req, res) =>{
  res.render('thankyou.ejs')
})

app.post('/order', jsonParser,(req, res) => {
  localStorage.setItem('orderData',JSON.stringify(req.body.data));
  let options = {
    amount: req.body.totalAmt * 100,
    currency: "INR",
    receipt: "Tea Order"
  };
  razorpay.orders.create(options, function(err, order) {
    
    res.json(order);
  })
})

app.post('/is-order-complete', (req, res) => {
  razorpay.payments.fetch(req.body.razorpay_payment_id).then((paymentResponse) => {
    if(paymentResponse.status === 'captured'){
      
      /* appeend data in order file */
      fs.appendFile('views/write/order-data.js', localStorage.getItem('orderData')+'\n', function (err) {
        localStorage.removeItem('orderData');
        if (err) throw err;
        console.log('Saved!');
      });

      /* read file data */
      fs.readFile('views/read/data.txt', 'utf8', function(err, data){
      
        // Display the file content
          console.log('data => ', data);
      });
        
      console.log('readFile called');
      res.redirect('/thank-you')
    }else{
      res.redirect('/')
    }
  })
})
app.listen(5000);